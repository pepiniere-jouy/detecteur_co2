/**************************************/
/* CO2 detector                       */
/**************************************/
/* Cyprien GUERIN                     */
/* MaIAGE & Pepiniere Numerique INRAE */
/**************************************/

/**********************/
/* Included libraries */
/**********************/
#include <Adafruit_NeoPixel.h>
#include <SparkFun_SCD30_Arduino_Library.h>
#include <WiFi.h>
#include <DNSServer.h>

/*********************/
/* Global parameters */
/*********************/

/* lights */
const int lights_pin = 26;
const int lights_nb = 4;
Adafruit_NeoPixel lights(lights_nb, lights_pin, NEO_RGB + NEO_KHZ800);

/* buzzer */
const int buzzer_pin = 27;
const int buzzer_channel = 0;
int buzzer_frequency = 2000;
int buzzer_resolution = 8;
unsigned long buzzer_last_short_bip = 0; // ms
int buzzer_short_bip_duration = 1000; //250; // ms
int buzzer_short_bip_interval = 1000 * 60; // ms
bool mute_buzzer = false;
int mute_buzzer_pushed_button = 1000 * 5; // ms
unsigned long mute_buzzer_button_last_pressed_time = 0;

/* CO2 sensor */
const int fixed_outside_reference = 400; // ppm (supposed value used at calibration)
SCD30 co2_sensor;
int level = -1;
int last_level = -1;
int theoretical_co2_mesured;
int co2_mesured_offset;
float temperature_mesure;
float humidity_mesure;

/* wifi access point */
const char* ssid     = "CO2_proto_01";
const byte dns_port = 53;
DNSServer dnsServer;
IPAddress apIP(192, 168, 1, 1);
WiFiServer server(80);

/* Button */
const int button_pin = 14;
int calibration_time_button_pushed = 0;
int buzzer_mute_time_button_pushed = 0;


/* Calibration */
int calibration_pushed_button = 1000 * 14; // ms
bool calibration_mode = false;
unsigned long calibration_button_last_pressed_time = 0;
const unsigned long wait_before_calibration = 1000 * 60 * 15; // ms
unsigned long wait_before_calibration_started = 0; // ms
bool calibration_started = false;
bool last_calibration_success = true;
bool last_calibration_time = 0;

/* General timing */
unsigned long current_time = 0;

/*********/
/* Setup */
/*********/
void setup() {
/* Serial communication */
  Serial.begin(115200);

/* Neopixels */
  lights.begin();
  lights.clear();
  lights.show();

/* buzzer */
  ledcSetup(buzzer_channel, buzzer_frequency, buzzer_resolution);
  ledcAttachPin(buzzer_pin, buzzer_channel);
  ledcWriteTone(buzzer_channel, buzzer_frequency);
  ledcWrite(buzzer_channel, 0);

/* CO2 sensor */
  Wire.begin();
  if (!co2_sensor.begin()) {
    Serial.println("The CO2 sensor seems missing");
    while (1);
  }

/* WiFi */
  WiFi.disconnect();   //added to start with the wifi off, avoid crashing
  WiFi.mode(WIFI_OFF); //added to start with the wifi off, avoid crashing
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP(ssid);
  dnsServer.start(dns_port, "*", apIP);
  server.begin();

/* Button */
  pinMode(button_pin, INPUT_PULLUP); // setup as pullup because button wired to GND

/* Light and sound demo */
  light_and_sound_demo(1);
}


/*************/
/* Main loop */
/*************/
void loop() {
  current_time = millis();

/* Button tracking */
  if(digitalRead(button_pin) == LOW){
    calibration_time_button_pushed++;
    buzzer_mute_time_button_pushed++;
  }else{
    calibration_time_button_pushed = 0;
    buzzer_mute_time_button_pushed = 0;
  }

/* Mute button tracking */
  if(buzzer_mute_time_button_pushed <= 1){
    mute_buzzer_button_last_pressed_time = current_time;
  }else if(mute_buzzer_button_last_pressed_time + mute_buzzer_pushed_button <= current_time){
    buzzer_mute_time_button_pushed = 0;
    buzzer_on_off_update();
  }

/* Calibration button tracking */
  if(!calibration_mode){
    if(calibration_time_button_pushed <= 1){
      calibration_button_last_pressed_time = current_time;
    }else if(calibration_button_last_pressed_time + calibration_pushed_button <= current_time){
      calibration_mode = true;
    }
  }
  
/* Calibration mode */
  if(calibration_mode){
    if(!calibration_started){
      wait_before_calibration_started = current_time;
      calibration_started = true;
      light_and_sound_demo(1);
    }
    if(calibration_started
       && wait_before_calibration_started + wait_before_calibration <= current_time){
      last_calibration_success = co2_sensor.setForcedRecalibrationFactor(fixed_outside_reference);
      last_calibration_time = current_time;
      calibration_started = false;
      calibration_mode = false;
      light_and_sound_demo(1);
    }
  }
  
/* CO2 sensor measures update */
  if(co2_sensor.dataAvailable()){
    theoretical_co2_mesured = co2_sensor.getCO2();
    co2_mesured_offset = theoretical_co2_mesured - fixed_outside_reference;
    temperature_mesure = co2_sensor.getTemperature();
    humidity_mesure = co2_sensor.getHumidity();

    last_level = level;
    level = CO2_ppm_offset_to_level(co2_mesured_offset);

    display_measures_on_serial();

  }

/* Update light and sound display */
  if(level != last_level){
    lighing_update(level);
    last_level = level;
  }

  if(!mute_buzzer){
    if(calibration_time_button_pushed > 0){
      ledcWrite(buzzer_channel, 255/4);
    }else if(level == 2
             && buzzer_last_short_bip + buzzer_short_bip_duration < millis()){
      ledcWrite(buzzer_channel, 0);
    }else{
      buzzer_last_short_bip = current_time - buzzer_short_bip_interval;
      buzzer_update(level);
    }
  }

/* HTTP request handling */
  dnsServer.processNextRequest();
  WiFiClient client = server.available();
  if(client){
    Serial.println("Incoming WiFi client");
    String currentLine = "";
    while(client.connected()){
      if(client.available()){
        char c = client.read();
        if(c == '\n'){
          if(currentLine.length() == 0){
            html_display_measures(client);
            break;
          }else{
            currentLine = "";
          }
        }else if(c != '\r'){
          currentLine += c;
        }
      }
    }
    client.stop();
  }
  
}


/*******************/
/* Other functions */
/*******************/

void light_and_sound_demo(int demo_cycle_nb){
  for(int i = 0; i < lights_nb * demo_cycle_nb; i++){
    lighing_update(i % lights_nb);
    ledcWrite(buzzer_channel, 0);
    if(i % lights_nb == lights_nb - 1){
      ledcWrite(buzzer_channel, 255 / 4);
    }
    delay(250);
  }
  lights.clear();
  lights.show();
  ledcWrite(buzzer_channel, 0);
}

void buzzer_on_off_update(void){
  mute_buzzer = !mute_buzzer;
  lights.clear();
  for(int i = 0; i < lights_nb; i++){
    if(mute_buzzer){
      lights.setPixelColor(i, lights.Color(100, 255, 0));
    }else{
      lights.setPixelColor(i, lights.Color(100, 0, 255));
    }
    lights.show();
    ledcWrite(buzzer_channel, 0);
    if(i % lights_nb == lights_nb - 1 && !mute_buzzer){
      ledcWrite(buzzer_channel, 255 / 4);
    }
    delay(250);
    lights.clear();
    lights.show();
  }
  ledcWrite(buzzer_channel, 0);
  if(!mute_buzzer){
    buzzer_update(level);
  }
  lighing_update(level);
}

void lighing_update(int tested_level){
  lights.clear();
  for(int j = 0; j <= tested_level; j++){
    if(tested_level == 0) {
      lights.setPixelColor(j, lights.Color(0, 0, 255));
    }else if(tested_level == 1){
      lights.setPixelColor(j, lights.Color(0, 255, 0));
    }else if(tested_level == 2){
      lights.setPixelColor(j, lights.Color(255, 255, 0));
    }else if(tested_level == 3){
      lights.setPixelColor(j, lights.Color(255, 0, 0));
    }
  }
  lights.show();
}

void buzzer_update(int tested_level){
  if(tested_level < 2){
    ledcWrite(buzzer_channel, 0);
  }else if(tested_level == 2){
    if(buzzer_last_short_bip + buzzer_short_bip_interval < millis()){
      ledcWrite(buzzer_channel, 255);
      buzzer_last_short_bip = millis();
    }
  }else if(tested_level > 2){
    ledcWrite(buzzer_channel, 255);
  }
}

void calibration(void){
  light_and_sound_demo(1);
}

int CO2_ppm_offset_to_level(int co2_mesured_offset){
  int calculated_level = -1;
  // Convert CO2 mesures to lighting levels
  if (co2_mesured_offset < 200) {
    calculated_level = 0;
  } else if (co2_mesured_offset < 400) {
    calculated_level = 1;
  } else if (co2_mesured_offset < 600) {
    calculated_level = 2;
  } else {
    calculated_level = 3;
  }
  return(calculated_level);
}

void display_measures_on_serial(void){
  Serial.println("-----------------------------------");
  Serial.println(String("uptime (s):                 ") + millis() / 1000);
  Serial.println(String("co2 offset (ppm):           ") + co2_mesured_offset + " (from calibration reference measure)");
  Serial.println(String("theoretical co2 (ppm):      ") + theoretical_co2_mesured + " (if calibrated at " + fixed_outside_reference + " ppm)");
  Serial.println(String("temperature (°C):           ") + temperature_mesure);
  Serial.println(String("humidity (%):               ") + humidity_mesure);
  Serial.println(String("risk level:                 ") + level);
  Serial.println(String("Last calibration time (ms): ") + last_calibration_time);
  String formated_calibration_success = "FALSE";
  if(last_calibration_success){
    formated_calibration_success = "TRUE";
  }
  Serial.println(String("Last calibration success: ") + formated_calibration_success);
  String formated_buzzer_mode = "ON";
  if(mute_buzzer){
    formated_buzzer_mode = "OFF";
  }
  Serial.println(String("Buzzer: ") + formated_buzzer_mode);
  if(calibration_started
       && wait_before_calibration_started + wait_before_calibration > current_time){
    Serial.println(String("Calibration in process."));
    Serial.println(String("Device should be outside at ") + fixed_outside_reference + " ppm.");
    unsigned int remaining_time = wait_before_calibration_started + wait_before_calibration - current_time;
    unsigned int remaining_minutes = remaining_time / (1000 * 60);
    unsigned int remaining_seconds = (remaining_time - (remaining_minutes * 60 * 1000)) / 1000;
    unsigned int remaining_milliseconds = remaining_time - ((remaining_minutes * 60 * 1000) + (remaining_seconds * 1000));
    String formated_remaining_time = String(remaining_minutes) + " min " + remaining_seconds + " sec " + remaining_milliseconds + " ms";
    Serial.println(formated_remaining_time + " remaining time before complete calibration.");
  }
}

void html_display_measures(WiFiClient client){
  client.println("HTTP/1.1 200 OK");
  client.println("Content-type:text/html");
  client.println("Connection: close");
  client.println();

  client.println("<!DOCTYPE html><html>");
  client.println("<head><title>D&eacute;tecteur CO2</title>");
  client.println("<h1>Voici les derni&egrave;res mesures du capteur CO2</h1><body><table>");
  client.println(String("<tr><th><b>temps d'activit&eacute; (s)</b></th><th>") + millis() / 1000);
  client.println(String("</th></tr><tr><th><b>&eacute;cart de co2 (ppm ; &eacute;cart &agrave; calibration)</b></th><th>") + co2_mesured_offset);
  client.println(String("</th></tr><tr><th><b>co2 th&eacute;orique (ppm ; si calibration &agrave; ") + fixed_outside_reference + " ppm)</b></th><th>" + theoretical_co2_mesured);
  client.println(String("</th></tr><tr><th><b>temp&eacute;rature (&#730;C)</b></th><th>") + temperature_mesure);
  client.println(String("</th></tr><tr><th><b>humidit&eacute; (%)</b></th><th>") + humidity_mesure);
  client.println(String("</th></tr><tr><th><b>niveau de risque (0 &agrave; 3)</b></th><th>") + level);
  client.println(String("</th></tr><tr><th><b>Temps de derni&egrave;re calibration (ms)</b></th><th>") + last_calibration_time);
  String formated_calibration_success = "&Eacute;chec";
  if(last_calibration_success){
    formated_calibration_success = "Succ&egrave;s";
  }
  client.println(String("</th></tr><tr><th><b>Derni&egrave;re calibration</b></th><th>") + formated_calibration_success);
  String formated_buzzer_mode = "Activ&eacute;";
  if(mute_buzzer){
    formated_buzzer_mode = "&Eacute;teint";
  }
  client.println(String("</th></tr><tr><th><b>Bip d'alerte</b></th><th>") + formated_buzzer_mode);
  
  client.println("</th></tr></table>");
  if(calibration_started
       && wait_before_calibration_started + wait_before_calibration > current_time){
    client.println("<br/>");
    client.println(String("<h1>Calibration en cour.</h1>"));
    client.println(String("Le d&eacute;tecteur doit &ecirc;tre dehors à ") + fixed_outside_reference + " ppm.<br/>");
    unsigned int remaining_time = wait_before_calibration_started + wait_before_calibration - current_time;
    unsigned int remaining_minutes = remaining_time / (1000 * 60);
    unsigned int remaining_seconds = (remaining_time - (remaining_minutes * 60 * 1000)) / 1000;
    unsigned int remaining_milliseconds = remaining_time - ((remaining_minutes * 60 * 1000) + (remaining_seconds * 1000));
    String formated_remaining_time = String(remaining_minutes) + " min " + remaining_seconds + " sec " + remaining_milliseconds + " ms";
    client.println(formated_remaining_time + " de temps restant avant compl&egrave;te calibration.");
  }
  client.println("</body></html>");
  client.println();
}
